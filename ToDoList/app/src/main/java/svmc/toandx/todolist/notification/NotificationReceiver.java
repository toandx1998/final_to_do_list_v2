package svmc.toandx.todolist.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class NotificationReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        int id = Integer.parseInt(intent.getStringExtra("ID"));
        Log.d("toandz","Notification Receiver "+Integer.toString(id));
        String title = intent.getStringExtra("NAME");
        String notes = intent.getStringExtra("NOTES");

        ShowNotification showNotification = new ShowNotification(id,title,notes,context);
        showNotification.CreateNotification();
    }
}
