package svmc.toandx.todolist.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import svmc.toandx.todolist.R;
import svmc.toandx.todolist.database.CustomDataBase;
import svmc.toandx.todolist.model.Task;
import svmc.toandx.todolist.notification.AlarmReceiver;
import svmc.toandx.todolist.notification.NotificationReceiver;
import svmc.toandx.todolist.notification.PriorityDialogFragment;
import svmc.toandx.todolist.notification.ReminderTypeDialogFragment;

public class EditTaskActivity extends AppCompatActivity implements ReminderTypeDialogFragment.IReminderTypeDialogListener, PriorityDialogFragment.IPriorityDialogListener{

    private EditText editTextNote,editTextTitle;
    private TextView dueDateTv,remindTypeTv,priorityTv;
    private Calendar dueTimeCalendar;
    private CheckBox statusCheckBox;
    CustomDataBase customDataBase;
    ArrayList<Task> tasks;
    Context mContext;
    SimpleDateFormat sdf;
    String title,note;
    int year,month,date,hour,minutes,subListID;
    Button submitButton;
    long dueTime,reminderTime;
    private boolean completed;
    private String reminderType = "None";
    private String priority = "Low";
    private DialogFragment dialogFragment;
    String oldRemindType;
    int oldStatus;
    Intent data;
    Task taskUpdate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_task);
        mContext=this;
        data=getIntent();
        subListID=data.getIntExtra("subListID",-1);
        customDataBase=CustomDataBase.getInstance(mContext);
        dueTimeCalendar=Calendar.getInstance();
        customDataBase=CustomDataBase.getInstance(this);
        dueTimeCalendar.set(Calendar.SECOND,0);
        dueDateTv=(TextView) findViewById(R.id.tv_click_time);
        editTextTitle=(EditText) findViewById(R.id.edt_title);
        editTextNote=(EditText) findViewById(R.id.edt_note);
        remindTypeTv=(TextView) findViewById(R.id.tv_reminder_type);
        priorityTv=(TextView) findViewById(R.id.tv_priority);
        submitButton=(Button) findViewById(R.id.btn_submit);
        statusCheckBox=(CheckBox) findViewById(R.id.checkbox_status);
        sdf=new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.getDefault());
        dueDateTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                year=dueTimeCalendar.get(Calendar.YEAR);
                month=dueTimeCalendar.get(Calendar.MONTH);
                date=dueTimeCalendar.get(Calendar.DATE);
                hour=dueTimeCalendar.get(Calendar.HOUR_OF_DAY);
                minutes=dueTimeCalendar.get(Calendar.MINUTE);
                Log.d("toandz","Init Time "+Integer.toString(hour)+" "+Integer.toString(minutes));
                DatePickerDialog datePickerDialog=new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int date) {
                        dueTimeCalendar.set(Calendar.YEAR,year);
                        dueTimeCalendar.set(Calendar.MONTH,month);
                        dueTimeCalendar.set(Calendar.DATE,date);
                        dueDateTv.setText(sdf.format(dueTimeCalendar.getTime()));
                    }
                },year,month,date);
                datePickerDialog.show();
                TimePickerDialog timePickerDialog=new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int minutes) {
                        dueTimeCalendar.set(Calendar.HOUR_OF_DAY,hour);
                        dueTimeCalendar.set(Calendar.MINUTE,minutes);
                        Log.d("toandz","Time "+Integer.toString(hour)+" "+Integer.toString(minutes));
                        dueDateTv.setText(sdf.format(dueTimeCalendar.getTime()));
                    }
                },hour,minutes,true);
                timePickerDialog.show();
            }
        });
        taskUpdate=new Task();
        if (data.getStringExtra("goal").equals("edit"))
        {
            taskUpdate.id=data.getIntExtra("taskID",0);
            taskUpdate=customDataBase.getTask(taskUpdate.id);
            Log.d("toandz","task reminder type "+taskUpdate.remindType);
            subListID=taskUpdate.subList_ID;
            oldRemindType=taskUpdate.remindType;
            oldStatus=taskUpdate.status;
            title=taskUpdate.title;
            editTextTitle.setText(title);
            note=taskUpdate.note;
            editTextNote.setText(note);
            reminderType=taskUpdate.remindType;
            remindTypeTv.setText(reminderType);
            dueTimeCalendar.setTimeInMillis(taskUpdate.dueTime);
            dueDateTv.setText(sdf.format(dueTimeCalendar.getTime()));
            priority=priorityFromIntToString(taskUpdate.priority);
            priorityTv.setText(priority);
            if (taskUpdate.status==1) statusCheckBox.setChecked(true); else statusCheckBox.setChecked(false);
        }
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                title=editTextTitle.getText().toString();
                note=editTextNote.getText().toString();
                dueTime=dueTimeCalendar.getTimeInMillis();
                Log.d("toandz","Due time "+Long.toString(dueTimeCalendar.getTimeInMillis())+" "+sdf.format(dueTimeCalendar.getTime()));
                taskUpdate.title=title;
                taskUpdate.note=note;
                taskUpdate.dueTime=dueTime;
                taskUpdate.subList_ID=subListID;
                taskUpdate.priority=priorityFromStringToInt(priority);
                taskUpdate.remindType=reminderType;
                int reqCode;
                if (statusCheckBox.isChecked()) taskUpdate.status=1; else taskUpdate.status=0;
                if (data.getStringExtra("goal").equals("edit"))
                {
                    customDataBase.updateTask(taskUpdate.id,taskUpdate);
                    reqCode=taskUpdate.id;
                    Log.d("toandz","remindType UpdateTask "+taskUpdate.remindType);
                    AlarmManager alarmManager1 = (AlarmManager)getSystemService(ALARM_SERVICE);
                    if (oldStatus==0) // Huy Notification, Alarm hien tai
                    switch (oldRemindType) {
                        case "None" :
                            break;
                        case "Notification":
                            Intent intent = new Intent(EditTaskActivity.this, NotificationReceiver.class);
                            intent.putExtra("ID",Integer.toString(reqCode));
                            intent.putExtra("NAME", title);
                            intent.putExtra("NOTES", note);
                            PendingIntent pendingIntent = PendingIntent.getBroadcast(EditTaskActivity.this, reqCode, intent,PendingIntent.FLAG_CANCEL_CURRENT );
                            alarmManager1.cancel(pendingIntent);
                            Log.d("toandz","Huy Notification "+Integer.toString(reqCode)+" "+Long.toString(dueTime));
                            break;
                        case "Alarm":
                            Intent intent1 = new Intent(EditTaskActivity.this, AlarmReceiver.class);
                            PendingIntent pendingIntent1 = PendingIntent.getBroadcast(EditTaskActivity.this,reqCode,intent1,PendingIntent.FLAG_CANCEL_CURRENT );
                            alarmManager1.cancel(pendingIntent1);
                            Log.d("toandz","Huy bao thuc "+Integer.toString(reqCode)+" "+Long.toString(dueTime));
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    reqCode=customDataBase.addTask(taskUpdate);
                }
                Log.d("toandz","Request code "+Integer.toString(reqCode));
                AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
                if (taskUpdate.status==0) // Tao Alarm, Notification moi
                switch (reminderType) {
                    case "None" :
                        break;
                    case "Notification":
                        Intent intent = new Intent(EditTaskActivity.this, NotificationReceiver.class);
                        intent.putExtra("ID",Integer.toString(reqCode));
                        intent.putExtra("NAME", title);
                        intent.putExtra("NOTES", note);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(EditTaskActivity.this,reqCode,intent,0);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                            alarmManager.setExact(AlarmManager.RTC_WAKEUP,dueTime, pendingIntent);
                        else alarmManager.set(AlarmManager.RTC_WAKEUP,dueTime,pendingIntent);
                        Log.d("toandz","Tao Notification "+Integer.toString(reqCode)+" "+Long.toString(dueTime));
                        break;
                    case "Alarm":
                        Intent intent1 = new Intent(EditTaskActivity.this, AlarmReceiver.class);
                        PendingIntent pendingIntent1 = PendingIntent.getBroadcast(EditTaskActivity.this,reqCode,intent1,0);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                            alarmManager.setExact(AlarmManager.RTC_WAKEUP,dueTime, pendingIntent1);
                        else alarmManager.set(AlarmManager.RTC_WAKEUP,dueTime,pendingIntent1);
                        Log.d("toandz","Tao bao thuc "+Integer.toString(reqCode)+" "+Long.toString(dueTime));
                        break;
                    default:
                        break;
                }
                finish();

            }
        });
        remindTypeTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showReminderTypeDialog();
            }
        });
        priorityTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPriorityDialog();
            }
        });

    }
    private void showReminderType() {
        remindTypeTv.setText(reminderType);
    }

    private void showPriority() {
        priorityTv.setText(priority);
    }

    @Override
    public void sendInput(String input) {
        reminderType = input;
        showReminderType();
    }

    @Override
    public void sendPriority(String input) {
        priority = input;
        showPriority();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) { }

    private void showReminderTypeDialog() {
        dialogFragment = new ReminderTypeDialogFragment();
        dialogFragment.show(getSupportFragmentManager(), "ReminderTypeDialogFragment");
    }

    private void showPriorityDialog() {
        dialogFragment = new PriorityDialogFragment();
        dialogFragment.show(getSupportFragmentManager(), "PriorityDialogFragment");
    }
    private int priorityFromStringToInt(String temp)
    {
        if (temp.equals("High")) return(1);
        if (temp.equals("Medium")) return(2);
        if (temp.equals("Low")) return(3);
        return(4);
    }
    private String priorityFromIntToString(int temp)
    {
        if (temp==1) return("High");
        if (temp==2) return("Medium");
        if (temp==3) return("Low");
        return("Low");
    }


}
